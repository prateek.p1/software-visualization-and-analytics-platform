import React from 'react';

interface LayoutProps {
  children: React.ReactNode;
}

export default function Layout(props: LayoutProps) {
  return <main className="w-full h-full p-8">
    <section className='p-4 border-2 rounded-xl flex flex-wrap'>
      {props.children}
    </section>
  </main>
}

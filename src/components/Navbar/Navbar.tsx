import React from 'react';
import { Link } from 'react-router-dom';
import appLogoImg from '../../../assets/images/app-logo.png';

export default function Navbar() {
  return (
    <header className="w-11/12 p-4 border-2 rounded-b-2xl">
      <nav className="w-full">
        <ul className="flex flex-wrap justify-between items-center">

          <li className="flex justify-center items-center">

            <img
              src={appLogoImg}
              className="w-20"
            />

            <a href="#" className="font-bold text-2xl flex flex-wrap">
              <div className='first-letter:text-blue-400 mr-1'>EyeROV </div>
              <div className='first-letter:text-blue-400 mr-1'>Visualization </div>
              <div className='first-letter:text-blue-400 mr-1'>and </div>
              <div className='first-letter:text-blue-400 mr-1'>Analytics </div>
              <div className='first-letter:text-blue-400'>Platform </div>
            </a>

          </li>

          <div className="flex flex-wrap justify-center items-center">

            <li>
              <Link to="/main_window" className="font-bold text-2xl mr-5">🏠Home</Link>
            </li>

            <li>
              <Link to="/faq" className="font-bold text-2xl mr-5">?FAQ</Link>
            </li>

            <li>
              <Link to="/support" className="font-bold text-2xl">🌐Support</Link>
            </li>

          </div>


        </ul>
      </nav>
    </header>
  );
}

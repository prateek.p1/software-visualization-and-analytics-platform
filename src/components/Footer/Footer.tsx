import { shell } from 'electron';
import React from 'react';

export default function Footer() {
  const eyeRovWebsite = 'https://eyerov.com';
  return (
    <footer className="absolute bottom-0 w-full p-4 border-2 bg-gray-800 text-gray-100">
      <nav>
        <ul className="flex flex-wrap justify-between items-center">
          <li>
            <a
              href="#"
              className="text-blue-500"
              onClick={(e) => shell.openExternal(eyeRovWebsite)}
            >www.eyerov.com</a>
            <p>info@eyerov.com</p>
          </li>
          <li>
            @2021 EyeROV (IROV Technologies Pvt. Ltd.)
          </li>
          <li>
            Version 1.0.0
          </li>
        </ul>
      </nav>
    </footer>
  )
}

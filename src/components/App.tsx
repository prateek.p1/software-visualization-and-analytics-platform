import React from 'react';
import { Route, Routes } from 'react-router-dom';
import Faq from './Faq/Faq';
import Footer from './Footer/Footer';
import Homepage from './Homepage/Homepage';
import Navbar from './Navbar/Navbar';
import NotFound from './NotFound/NotFound';
import Support from './Support/Support';

export default function App() {
  return (
    <>
      <Navbar />
      <Routes>
        <Route path="/main_window" element={<Homepage />} />
        <Route path="/support" element={<Support />} />
        <Route path="/faq" element={<Faq />} />
        <Route path="*" element={<NotFound />} />
      </Routes>
      <Footer />
    </>
  )
}

import React from 'react';

export default function NotFound() {
  return (
    <div className="w-full h-full flex justify-center items-center">
      <h1 className="text-5xl">404</h1>
    </div>
  )
}

import React from 'react';
import Layout from '../Layout/Layout';

export default function Faq() {
  return (
    <Layout>
      <div className="w-full mb-10">
        <h1 className="text-2xl text-gray-600 font-bold">Frequently Asked Questions</h1>
      </div>

      <section className="p-3 h-36 flex flex-col justify-between">
        <p className="text-xl">
          For Technical Support, please mail us at - {' '}
          <a
            href="mailto:support@eyerov.com"
            className="text-blue-500 hover:text-pink-400"
          >support@eyerov.com</a>
        </p>
        <p className="text-xl">
          For Sales Support, please mail us at - {' '}
          <a
            href="mailto:sales@eyerov.com"
            className="text-blue-500 hover:text-pink-400"
          >sales@eyerov.com</a>
        </p>
        <p className="text-xl">
          For All Other Queries, please mail us at - {' '}
          <a
            href="info@eyerov.com"
            className="text-blue-500 hover:text-pink-400"
          >info@eyerov.com</a>
        </p>
      </section>
    </Layout>
  )
}
